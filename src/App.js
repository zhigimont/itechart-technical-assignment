import {
    BrowserRouter,
    Route
} from 'react-router-dom';
import React, { Component } from 'react';
import './App.css';
import Counter from './counter/Counter';
import Creator from './creator/Creator';
import List from './list/List';
import items from './mock/items';
import Details from './details/Details';
var _ = require('underscore');

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cart: items
        };

        this.handleAddToCart = this.handleAddToCart.bind(this);
    }

    handleAddToCart(item) {
        var _id = _(this.state.cart)
            .chain()
            .pluck('id')
            .max()
            .value();

        this.state.cart.push(_(item).extend({
            id: _(_id).isFinite() ? ++_id : 1
        }));

        this.setState({
            cart: this.state.cart
        });

    }

    handleRemoveFromCart(id) {
        this.setState({
            cart: _(this.state.cart).reject({
                id: id
            })
        });
    }

    handleModifyInCart(updatedItem) {
        _(this.state.cart)
            .chain()
            .findWhere({id: updatedItem.id})
            .extend(updatedItem);

        this.setState(this.state.cart);
    }

    render() {
        const DetailsData = ({ match }) => {
            var _product = _(this.state.cart).find({id: +match.params.id});

            return _product ? <Details product={_product}/> : <p className="title">No product found</p>;
        };

        const NoMatch = ({ location }) => (
            <div>
                <p className="title">No match for <code>{location.pathname}</code></p>
            </div>
        );

        return (
            <BrowserRouter>
                <div className="App">
                    <div className="App-l">
                        <Creator addToCart={this.handleAddToCart}/>
                    </div>
                    <div className="App-r">
                        <Route exact path="/"
                               component={() => <List list={this.state.cart}
                                modifyInCart={this.handleModifyInCart.bind(this)}
                                removeFromCart={this.handleRemoveFromCart.bind(this)}/>}/>
                        <Route path="/product/:id" component={DetailsData}/>
                        <Route component={NoMatch}/>
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
