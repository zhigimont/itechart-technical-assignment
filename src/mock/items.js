const data = [
    {
        id: 1,
        name: 'Snow',
        price: 10,
        quantity: 5,
        type: 1
    }, {
        id: 2,
        name: 'Gift',
        price: 12,
        quantity: 10,
        type: 3
    }
];

export default data;