import React, { Component } from 'react';
import './Details.css';
import {Link} from 'react-router-dom';
import SelectorTypes from '../selector/Types';
var _ = require('underscore');

class Details extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var icon = _(SelectorTypes).find({id: this.props.product.type});

        return (
            <div className="Details">
                <p className="title">{this.props.product.name}</p>
                <ul>
                    <li>
                        <span className="itype">{icon.icon}</span>
                    </li>
                    <li>Count: {this.props.product.quantity}</li>
                    <li>Price: {this.props.product.price} $</li>
                    <li>Total: {this.props.product.quantity * this.props.product.price} $</li>
                </ul>
                <div>
                    <Link to="/">Go back</Link>
                </div>
            </div>
        )
    }
}

export default Details;