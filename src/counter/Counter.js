import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Counter.css';

class Counter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.quantity
        };
    }

    increment(e) {
        e.preventDefault();

        this.setState(prev => ({
            value: Number(prev.value) + 1
        }), () => {
            this.props.updateQuantity(this.state.value);
        });
    };

    decrement(e) {
        e.preventDefault();

        if (this.state.value <= 1) {
            return this.state.value;
        }
        else {
            this.setState(prev => ({
                value: Number(prev.value) - 1
            }), () => {
                this.props.updateQuantity(this.state.value);
            });
        }
    };

    componentWillReceiveProps(nextProps) {
       if(this.state.value !== nextProps.quantity) {
           this.setState({
               value: nextProps.quantity
           });
       }
    }

    render() {
        return (
            <div className="Counter">
                <a href="#" className="Counter-decrease" onClick={this.decrement.bind(this)}>–</a>
                <span className="Counter-quantity">{this.state.value}</span>
                <a href="#" className="Counter-increase" onClick={this.increment.bind(this)}>+</a>
            </div>
        )
    }
}

Counter.propTypes = {
    value: PropTypes.number
};

export default Counter;
