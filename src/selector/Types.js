import React, { Component } from 'react';
import CartIcon from 'react-icons/lib/md/shopping-cart';
import SnowIcon from 'react-icons/lib/md/ac-unit';
import CakeIcon from 'react-icons/lib/md/cake';
import GiftIcon from 'react-icons/lib/go/gift';
import ListIcon from 'react-icons/lib/fa/pagelines';

const SELECTOR_TYPES = [{
    id: 0,
    icon: <CartIcon />
}, {
    id: 1,
    icon: <SnowIcon />
}, {
    id: 2,
    icon: <CakeIcon />
}, {
    id: 3,
    icon: <GiftIcon />
}, {
    id: 4,
    icon: <ListIcon />
}];

export default SELECTOR_TYPES;