import React, { Component } from 'react';
import './Selector.css';
import SelectorTypes from './Types';
var _ = require('underscore');


class Selector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
            selectedType: this.props.default
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.selectedType !== nextProps.default) {
            this.setState({
                selectedType: nextProps.default
            });
        }
    }

    trigger(e) {
        this.setState({
            expanded: !this.state.expanded
        });
    }

    select(type) {
        this.setState({
            selectedType: type,
            expanded: false
        });
    }

    render() {
        let _typesList = SelectorTypes.map((item) => {
            return (
                !_(item).isEqual(this.state.selectedType) ? <a className="itype" key={item.id} onClick={this.select.bind(this, item)}>
                    {item.icon}
                </a> : null
            );
        });

        let _dropdown = this.state.expanded ? (
            <div className="Selector-dropdown">
                {_typesList}
            </div>
        ) : null;

        return (
            <div className="Selector">
                <div>
                    <a className="itype" onClick={this.trigger.bind(this)}>
                        {this.state.selectedType.icon}
                    </a>
                </div>
                {_dropdown}
            </div>
        );
    }
}

export default Selector;
