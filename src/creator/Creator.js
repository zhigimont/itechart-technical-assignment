import React, { Component } from 'react';
import './Creator.css';
import Counter from '../counter/Counter';
import Selector from '../selector/Selector';
import SelectorTypes from '../selector/Types';

const DEFAULT_TYPE = SelectorTypes[0];

const INITIAL_STATE = {
    isAddAllow: false,
    productName: '',
    productPrice: '',
    productQuantity: 1,
    productType: DEFAULT_TYPE.id
};

class Creator extends Component {
    constructor(props) {
        super(props);

        this.state = INITIAL_STATE;
    }

    nameChange(e) {
        this.setState({
            productName: e.target.value,
            isAddAllow: e.target.value.length && +this.state.productPrice > 0
        });
    }

    priceChange(e) {
        +e.target.value > 0 && this.setState({productPrice: +e.target.value});
        e.target.value === '' && this.setState({productPrice: e.target.value});

        this.setState({
            isAddAllow: this.state.productName.length && (+e.target.value > 0 || e.target.value.length)
        });
    }

    add(e) {
        this.props.addToCart({
            name: this.state.productName,
            price: +this.state.productPrice,
            quantity: +this.state.productQuantity,
            type: +this.state.productType
        });

        this.setState(INITIAL_STATE);
    }

    onQuantityUpdate(newValue) {
        this.setState({
            productQuantity: newValue
        });
    }

    render() {
        return (
            <div className="Creator">
                <p className="title">Add product to your cart list</p>
                <div className="Creator-block">
                    <input placeholder="Product name" value={this.state.productName}
                           onChange={this.nameChange.bind(this)}/>
                </div>
                <div className="Creator-block">
                    <input placeholder="Product price" value={this.state.productPrice}
                           onChange={this.priceChange.bind(this)}/>
                </div>
                <div className="Creator-block">
                    <Counter quantity={this.state.productQuantity} updateQuantity={this.onQuantityUpdate.bind(this)} />
                </div>
                <div className="Creator-block">
                    <Selector default={DEFAULT_TYPE} />
                </div>
                <div className="Creator-block">
                    <button className="Creator-add" disabled={!this.state.isAddAllow} onClick={this.add.bind(this)}>
                        Add to list
                    </button>
                </div>
            </div>
        );
    }
}

export default Creator;
