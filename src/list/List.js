import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './List.css';
import Product from './Product';
var _ = require('underscore');


class List extends Component {
    constructor(props) {
        super(props);
    }

    summary() {
        return _(this.props.list)
            .chain()
            .map((item) => item.quantity * item.price)
            .reduce((memo, num) => memo + num, 0)
            .value();
    }

    render() {
        let productsData = this.props.list.map((product) => {
            return (
                <Product key={product.id}
                         product={product}
                         modifyInCart={this.props.modifyInCart}
                         removeFromCart={() => this.props.removeFromCart(product.id)}/>
            )
        });

        return (
            <div className="List">
                <p className="title">Product list</p>
                {productsData}
                <p className="List-summary">Total: {this.summary()} $</p>
            </div>
        )
    }
}

List.propTypes = {
    value: PropTypes.number
};

export default List;
