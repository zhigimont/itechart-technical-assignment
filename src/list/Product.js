import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Counter from '../counter/Counter';
import RemoveIcon from 'react-icons/lib/ti/trash';
import DetailsIcon from 'react-icons/lib/md/info-outline';
import './Product.css';
import {Link} from 'react-router-dom';
import SelectorTypes from '../selector/Types';
var _ = require('underscore');

class Product extends Component {
    constructor(props) {
        super(props);
    }

    onQuantityUpdate(newValue) {
        var _product = this.props.product;

        _product.quantity = newValue;

        this.props.modifyInCart(_product);
    }

    render() {
        let icon = _(SelectorTypes).find({id: this.props.product.type});

        return (
            <div className="List-product">
                <div className="List-product-block">
                    <span className="itype">{icon.icon}</span>
                </div>
                <div className="List-product-block">
                    <p className="List-product-title">{this.props.product.name}</p>
                    <Counter quantity={this.props.product.quantity} updateQuantity={this.onQuantityUpdate.bind(this)}/>
                    <p className="List-product-total">Total: {this.props.product.quantity * this.props.product.price}
                        $</p>
                </div>
                <div className="List-product-block">
                    <a className="List-product-button List-product-button_remove"
                       onClick={this.props.removeFromCart}>
                        <RemoveIcon />
                    </a>
                    <Link className="List-product-button List-product-button_details"
                          to={'/product/' + this.props.product.id}><DetailsIcon />
                    </Link>
                </div>
            </div>
        )
    }
}

export default Product;
